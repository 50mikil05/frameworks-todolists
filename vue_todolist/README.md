# my-project

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Этапы разработки:

1) Делаем компонент TodoList.vue в котором будут отображаться TodoListItem, в props этого компонента попадает массив notes []
2) Делаем компонент TodoListItems.vue в который переносим верстку карточки todo
3) В props TodoListItems.vue должен поступать объект карточки ``{ title, description, isComplete: false }``
4) Делаем компонент TodoForm.vue, в котором будет форма добавления/редактирования заметки. Переносим в него верстку,
## Дальше созвон
