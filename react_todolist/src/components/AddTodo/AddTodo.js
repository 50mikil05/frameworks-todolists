import React, { useState } from "react";
function AddTodo({ setTodo, todo }) {
  const [value, setValue] = useState("");
  function saveTodo() {
    setTodo([
      ...todo,
      {
        id: Math.random().toString(36).substring(2, 9),
        content: value,
      },
    ]);
  }
  return (
    <div class="input-group mb-3">
      <button
        onClick={saveTodo}
        class="btn btn-outline-light note-add"
        type="button"
        id="button-addon1"
      >
        добавить заметку
      </button>
      <input
        value={value}
        onChange={(e) => setValue(e.target.value)} //onChange, с помощью него мы передаём полученное значение в setValue
        placeholder="Введите название заметки"
        type="text"
        id="note"
        class="form-control"
        aria-label="Example text with button addon"
        aria-describedby="button-addon1"
      />
    </div>
  );
}
export default AddTodo;
