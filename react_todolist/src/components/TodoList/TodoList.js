import React from "react";

function TodoList({ todo, setTodo }) {
  function deleteTodo(id){
    let newTodo= [...todo].filter(item=>item.id!=id)//записываем переменную в наш массив, если айди не равен полученному от кнопки 
    setTodo(newTodo)//в setTodo передаём новую переменную, setTodo обновляет значение todo
  }
  return (
    <span class="text-light notes">
      <div id="container">
        {todo.map((item) => (
          <div key={item.id}>
            <div>{item.content}</div>
            <button onClick={()=>deleteTodo(item.id)} type="button" class="btn btn-secondary btn-sm ">
                  удалить заметку
                </button>
      <hr class="bg-light" /> 

          </div>     
        ))}
      </div>

    </span>
    
  );
}
export default TodoList;
