import { useState } from 'react';
import './App.css';
import AddTodo from './components/AddTodo/AddTodo';
import TodoList from './components/TodoList/TodoList';

function App() {

  const [todo,setTodo]= useState([
  
  ])
  return (

    <body>
    <div class="container-fluid" id="app">
      <div class="card bg-dark">
        <div class="card-body">
          <h1 class="text-light">
            Список заметок
          </h1>

      <AddTodo todo={todo} setTodo={setTodo}/>
          <hr class="bg-light" /> 
          <ul class="list-group">
            <li>
              {/* заметки */}      
             <TodoList todo={todo} setTodo={setTodo}/>
              <div class="d-grid gap-2 d-md-block note-actions">
              
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </body>
  );
}

export default App;
