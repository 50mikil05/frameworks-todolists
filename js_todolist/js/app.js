function getNoteCart(note, index) {
  const checked = note.isComplete ? 'checked':'';
  return `<div>
    <hr class="hr_color" />
    <div class="note__flex">
          <button class="btn__reset" onclick="editNote(${index})" id="svg__edit">
            <svg class="svg__main" id="svg__edit" viewBox="0 0 330 330" fill="currentColor">
              <path id="XMLID_24_" d="M75,180v60c0,8.284,6.716,15,15,15h60c3.978,0,7.793-1.581,10.606-4.394l164.999-165
              c5.858-5.858,5.858-15.355,0-21.213l-60-60C262.794,1.581,258.978,0,255,0s-7.794,1.581-10.606,4.394l-165,165
              C76.58,172.206,75,176.022,75,180z M105,186.213L255,36.213L293.787,75l-150,150H105V186.213z" />
              <path id="XMLID_27_"
                d="M315,150.001c-8.284,0-15,6.716-15,15V300H30V30H165c8.284,0,15-6.716,15-15s-6.716-15-15-15H15
              C6.716,0,0,6.716,0,15v300c0,8.284,6.716,15,15,15h300c8.284,0,15-6.716,15-15V165.001C330,156.716,323.284,150.001,315,150.001z" />
            </svg>
            </button>
          <div  class="note__card">
            <p class="tittle__note">${note.title}</p>
            <p class="description__note">${note.description}</p>
            <input type="checkbox" onclick="functionName(event)" data-index="${index}" ${checked}>
          </div>
          <button class="btn__reset" onclick="deleteNotes(${index})" id="svg__delete">
          <svg class="svg__main"  viewBox="0 0 473 473" fill="currentColor">
            <path d="M324.285,215.015V128h20V38h-98.384V0H132.669v38H34.285v90h20v305h161.523c23.578,24.635,56.766,40,93.477,40
              c71.368,0,129.43-58.062,129.43-129.43C438.715,277.276,388.612,222.474,324.285,215.015z M294.285,215.015
              c-18.052,2.093-34.982,7.911-50,16.669V128h50V215.015z M162.669,30h53.232v8h-53.232V30z M64.285,68h250v30h-250V68z M84.285,128
              h50v275h-50V128z M164.285,403V128h50v127.768c-21.356,23.089-34.429,53.946-34.429,87.802c0,21.411,5.231,41.622,14.475,59.43
              H164.285z M309.285,443c-54.826,0-99.429-44.604-99.429-99.43s44.604-99.429,99.429-99.429s99.43,44.604,99.43,99.429
              S364.111,443,309.285,443z" />
          </svg>
        </button>
        </div>
        </div>`;
}

/**
 * Функция для изменения состояния isComplete на клик по чекбоксу
 ** 1 - добавить на чекбокс параметр data-index в котором будет лежать index элемента из массива
 * *2 - функцию можно повесить на элемент onclick="functionName", а можно и повесить глобальный слушатель с уточнением на элемент
 * *3 - функция принимает значание event
 * *4 - из event можно достать элемент по которому произошел клик, вытащить из него index,
 *  *  прочитать в каком состоянии сейчас чекбок (checked || !checked),
 *   * записать значение isComplete в элемент массива notes по индексу и сохранить все LS *
 */

/**
 * НОВОЕ
 * Обрати внимание на functionName. что твориться в event(распечатай посмотри)
 * посмотри на строке 19
 * посмотри как можно читать и записывать в атрибуты DOM элеметов
 * @type {*[]}
 */
let notes = [];//массив с заметками

function functionName(event) {
  const input = event.target;
  const isComplete = input.checked;
  const index = input.getAttribute('data-index')
  notes[index].isComplete = isComplete;
  saveToLocalStorage();
}
// } //функция собирает данные с формы
function getFormValue() {
  //записываем в переменную ноду дом дерева
  let titleNode = document.getElementById("tittle__note");
  let descriptionNode = document.getElementById("description__note");
  //получаем данные заголовка, trim удаляет лишние пробелы, операторы '?' для проверки истинности заголовка
  let title = (titleNode?.value ?? "").trim();
  let description = (descriptionNode?.value ?? "").trim();
  //это результат выполнения функции, то, что она вернёт через return
  return { title, description, isComplete: false };
}
//сохранение и добавление заметки
function saveToNotes() {
  //отменяет перезагрузку страницы после нажатие на ссылку
  event.preventDefault();

  const note = getFormValue();
  //если нет заголовка, будет выполнен алерт, если заголовок есть, то заметка добавится в массив
  if (!note.title) return alert("Нет названия");
  {
    notes.push(note);
    //очистка поля ввода
    document.getElementById("tittle__note").value = "";
    document.getElementById("description__note").value = "";
    updateView();
    saveToLocalStorage();
  }
}
function editNote(index) {
  let eDNote = notes.splice(index, 1);
  //вводим в форму данные, полученные выше
  document.getElementById("tittle__note").value = eDNote[0].title;
  document.getElementById("description__note").value = eDNote[0].description;
  updateView();
  saveToLocalStorage();
}
//функция для удаления заметки
function deleteNotes(index) {
  notes.splice(index, 1);
  updateView();
  saveToLocalStorage();
}

function updateView() {
  const notesContainer = document.querySelector(".notes__items");
  //*const notesElems = notes.map((note, index) => getNoteCart(note, index));
  const notesElems = notes.map(getNoteCart);
  notesContainer.innerHTML = notesElems.join("");
}

function saveToLocalStorage() {
  let parsed = JSON.stringify(notes);
  localStorage.setItem("notes", parsed);
}

function restoreNotes(){
  let newNote = JSON.parse(localStorage.getItem("notes"));
  if (!newNote || !newNote?.length) return;
  notes = newNote;
  updateView();
}

(function () {
  restoreNotes();
})();

// (function () {
//   const arr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
//   for (let i = 0; i < arr.length; i++) {
//       if (i === 0) continue;
//       arr[i] = arr[i - 1] + 1;
//       console.log(arr[i])
//   }
  
// })();
